# Server-Website-Pemrograman-Jaringan

-Menggunakan pustaka socket biasa

-Buatlah HTTP server yang menangani request dari klien.

-Buatlah satu halaman html dan satu halaman php di sisi server.

-Halaman html dan php inilah yang di-request oleh klien.

-Buatlah direktori dataset yang berisi file-file (seperti Tugas01). Selanjutnya, klien bisa masuk dengan mengkliknya, melihat list file, dan mengunduh file di dalam direktori dataset.

-Klien bisa melihat daftar isi direktori 'dataset' karena tidak ada file index.html di dalamnya.

-Respon yang didukung oleh server adalah 200 OK dan 404 Not Found (bisa ditambahkan yang lain).

-Server dapat diakses dengan browser client yang umum digunakan, misalnya Firefox dan Chrome.

-Port server yang digunakan server (misalnya port 80) harus disimpan di dalam file terpisah, tidak boleh disimpan dalam source code.

-Klien dapat mengakses file PHP dan menampilkan isi script hasil pengolahan PHP (Hint: bisa menggunakan pemanggilan perintah shell PHP-CLI dari Python).

-Struktur direktori server:

- server
     - dataset
     - index.html 
     - test.php
     - server.py
     - httpserver.conf
     - 404.html
