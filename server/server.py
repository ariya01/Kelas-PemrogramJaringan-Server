import socket
import sys
import select
import re
import os, os.path
import subprocess
import urllib2

f = open('httpserver.conf', 'r')
response_port = f.read()
f.close()
response_port2 = response_port.split()[1]
# print response_port2

server_address = ('127.0.0.1', int(response_port2))
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(server_address)
server_socket.listen(5)

input_socket = [server_socket]

try:
	while True:
		read_ready, write_ready, exception = select.select(input_socket, [], [])

		for sock in read_ready:
			if sock == server_socket:
				client_socket, client_address = server_socket.accept()
				input_socket.append(client_socket)

			else:
				data = sock.recv(4096)

				# print data
				
				request_header = data.split('\r\n')
				request_file = request_header[0].split()[1]
				
				# match = re.match('GET /index.html\sHTTP/1', data)
				# if match:
				# 	sock.sendall("""HTTP/1.0 200 OK
				# 	Content-Type: text/html

				# 	<html>
				# 	<head>
				# 	<title>Success</title>
				# 	</head>
				# 	<body>
				# 	Boo!
				# 	</body>
				# 	</html>
				# 	""")
				# else:
				# 	sock.sendall("""HTTP/1.0 404 NOT FOUND
				# 	Content-Type: text/html

				# 	<html>
				# 	<head>
				# 	<title>Success</title>
				# 	</head>
				# 	<body>
				# 	KENTOT
				# 	</body>
				# 	</html>
				# 	""")

				# print request_file
				if request_file == '/index.html' or request_file == '/' or request_file == 'index':
					f = open('index.html', 'r')
					response_data = f.read()
					f.close()
					content_length = len(response_data)
					response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(content_length) + '\r\n\r\n'
				elif request_file == '/test.php' or request_file == '/test':
					# if the script don't need output.
					# subprocess.call("php /path/to/your/script.php")

					# if you want output
					proc = subprocess.Popen("php test.php", shell=True, stdout=subprocess.PIPE)
					response_data = str(proc.stdout.read())
					content_length = len(response_data)
					response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(
                        content_length) + '\r\n\r\n'
					input_socket.remove(sock)
				elif request_file == '/dataset':
					flag =0;
					for dir, subdir, files in os.walk('../server/dataset'):
						for file in files:
							# print os.path.join(file)
							if os.path.join(file) == 'index.html':
								flag=1
					if flag ==1:
						f = open('index.html', 'r')
						response_data = f.read()
						f.close()
						content_length = len(response_data)
						response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(content_length) + '\r\n\r\n'	 
					else:
						response_data = '<html><head><title>Dataset List</title></head><body>'
						for dir, subdir, files in os.walk('dataset'):
							for file in files:
								response_data = response_data+'<a href="dataset/'+os.path.join(file)+'">'+os.path.join(file)+'</a>'+'<br>'
						response_data = response_data+'</body></html>'
						content_length = len(response_data)
						response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(content_length) + '\r\n\r\n'

				else:
					request_file = urllib2.unquote(request_file)
					directory = os.getcwd() + request_file
					if os.path.exists(directory):
						with open(directory, "rb") as f:
							response_data= f.read(1024)
							while response_data:
								if(sock.send(response_data)):
									response_data = f.read(1024)
						content_length = len(response_data)
						response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(content_length) + '\r\n\r\n'
					else:
						f = open('404.html', 'r')
						response_data = f.read()
						f.close()

						content_length = len(response_data)
						response_header = 'HTTP/1.1 404 NOT FOUND\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' + str(content_length) + '\r\n\r\n'

				# print response_header + response_data
				sock.sendall(response_header + response_data)
				# sock.close()
except KeyboardInterrupt:
	server_socket.close()
	sys.exit(0)